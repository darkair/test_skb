import { Component, OnInit, Input } from '@angular/core';

import { Task } from '../task/task';

@Component({
  selector: 'task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.less'],
  providers: []
})
export class TaskManagerComponent implements OnInit {
  @Input() title: string;

  showForm: boolean = false;
  showView: boolean = false;
  currentTask: Task = null;

  ngOnInit(): void {
  }

  newTask(): void {
    this.currentTask = null;
    this.showForm = true;
    this.showView = false;
  }

  viewTask(task: Task): void {
    this.currentTask = task;
    this.showView = true;
    this.showForm = false;
  }

  editTask(task: Task): void {
    this.currentTask = task;
    this.showForm = true;
    this.showView = false;
  }

  closeTaskForm(): void {
    this.showForm = false;
  }

  closeTaskView(): void {
    this.showView = false;
  }
}