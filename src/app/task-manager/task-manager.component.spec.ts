import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { NgbModule }                from '@ng-bootstrap/ng-bootstrap';
import { DragulaModule }            from 'ng2-dragula';
import { StorageServiceModule }     from 'angular-webstorage-service';
import { TaskManagerComponent }     from './task-manager.component';
import { TaskListComponent }        from './../task-list/task-list.component';
import { TaskFormComponent }        from './../task-form/task-form.component';
import { TaskViewComponent }        from './../task-view/task-view.component';

describe('TaskManagerComponent', () => {
  let component: TaskManagerComponent;
  let fixture: ComponentFixture<TaskManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        DragulaModule.forRoot(),
        StorageServiceModule
      ],
      declarations: [
        TaskManagerComponent,
        TaskListComponent,
        TaskFormComponent,
        TaskViewComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
