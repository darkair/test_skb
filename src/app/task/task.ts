export class Task {
  title: string = '';
  desc: string = '';
  date: Date = new Date();
  done: boolean = false;
}
