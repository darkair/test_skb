import { BrowserModule }            from '@angular/platform-browser';
import { NgModule }                 from '@angular/core';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { NgbModule }                from '@ng-bootstrap/ng-bootstrap';
import { DragulaModule }            from 'ng2-dragula';
import { StorageServiceModule }     from 'angular-webstorage-service';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';

import { AppRoutingModule }         from './app-routing.module';
import { AppComponent }             from './app.component';
import { VarDirective }             from './directives/var';
import { TaskManagerComponent }     from './task-manager/task-manager.component';
import { TaskListComponent }        from './task-list/task-list.component';
import { TaskFormComponent }        from './task-form/task-form.component';
import { TaskViewComponent }        from './task-view/task-view.component';

@NgModule({
  declarations: [
    AppComponent,
    VarDirective,
    TaskManagerComponent,
    TaskListComponent,
    TaskFormComponent,
    TaskViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DragulaModule.forRoot(),
    StorageServiceModule,
    BrowserAnimationsModule
  ],
  exports: [
    AppComponent,
    TaskManagerComponent,
    TaskListComponent,
    TaskFormComponent,
    TaskViewComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
