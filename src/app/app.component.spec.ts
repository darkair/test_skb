import { TestBed, async }           from '@angular/core/testing';
import { RouterTestingModule }      from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { NgbModule }                from '@ng-bootstrap/ng-bootstrap';
import { DragulaModule }            from 'ng2-dragula';
import { StorageServiceModule }     from 'angular-webstorage-service';
import { AppComponent }             from './app.component';
import { TaskManagerComponent }     from './task-manager/task-manager.component';
import { TaskListComponent }        from './task-list/task-list.component';
import { TaskFormComponent }        from './task-form/task-form.component';
import { TaskViewComponent }        from './task-view/task-view.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        DragulaModule.forRoot(),
        StorageServiceModule
      ],
      declarations: [
        AppComponent,
        TaskManagerComponent,
        TaskListComponent,
        TaskFormComponent,
        TaskViewComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Task manager'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Task manager');
  });
});
