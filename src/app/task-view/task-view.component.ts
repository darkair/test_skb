import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { animate, transition, state, trigger, style } from '@angular/animations';
import { Task } from '../task/task';

@Component({
  selector: 'task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.less'],
  animations: [
    trigger('openClose', [
      state('void', style({
        maxHeight: '0px'
      })),
      state('*', style({
        maxHeight: '600px'
      })),
      transition(':enter', animate('400ms')),
      transition(':leave', animate('300ms'))    
    ])      
  ]
})
export class TaskViewComponent implements OnInit {
  @Input() task: Task = new Task();
  @Output() close = new EventEmitter<boolean>();

  currentState: string = 'void';     // Current state of animation

  constructor() { }

  ngOnInit() {
    this.currentState = 'open';
  }

  animationDone($event) {
    if (this.currentState === 'void') {
      this.close.emit(true);
    }
  }

  closeForm(): void {
    this.currentState = 'void';
  }
}
