import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { NgbModule }                from '@ng-bootstrap/ng-bootstrap';
import { StorageServiceModule }     from 'angular-webstorage-service';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { TaskFormComponent }        from './task-form.component';

describe('TaskFormComponent', () => {
  let component: TaskFormComponent;
  let fixture: ComponentFixture<TaskFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        StorageServiceModule,
        BrowserAnimationsModule
      ],
      declarations: [
        TaskFormComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
