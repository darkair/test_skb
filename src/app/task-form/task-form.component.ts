import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { animate, transition, state, trigger, style } from '@angular/animations';

import { Task } from '../task/task';
import { TaskService } from '../services/task.service';

@Component({
  selector: 'task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.less'],
  animations: [
    trigger('openClose', [
      state('void', style({
        maxHeight: '0px'
      })),
      state('*', style({
        maxHeight: '1000px',
        overflow: 'unset'
      })),
      transition(':enter', animate('200ms')),
      transition(':leave', animate('200ms'))
    ])      
  ]
})
export class TaskFormComponent implements OnInit, OnChanges {
  @Input() task: Task = new Task();
  @Output() close = new EventEmitter<boolean>();

  form: FormGroup;
  isCreatingForm: boolean = false;   // Whether this is an creating or an editing form
  currentState: string = 'void';     // Current state of animation

  constructor(
    private taskService: TaskService,
    private calendar: NgbCalendar
  ) {}

  ngOnInit(): void {
    if (this.task === null) {
      this.task = new Task();
      this.isCreatingForm = true;
    }
    this.form = new FormGroup({
      'title': new FormControl(this.task.title, [
        Validators.required
      ]),
      'desc': new FormControl(this.task.desc),
      'date': new FormControl(
        (this.task.date
          ? this.fromDate(this.task.date)
          : this.calendar.getToday()
        ), [
          Validators.required
        ]
      )
    });

    this.currentState = 'open';
  }

  get title() { return this.form.get('title'); }
  get desc() { return this.form.get('desc'); }
  get date() { return this.form.get('date'); }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form === undefined) {
      return;
    }
    const task: SimpleChange = changes.task;
    const currentTask: Task = task.currentValue;
    if (currentTask !== null) {
      this.title.setValue(currentTask.title);
      this.desc.setValue(currentTask.desc);
      this.date.setValue(this.fromDate(currentTask.date));
      this.isCreatingForm = false;
    }
  }

  animationDone($event) {
    if (this.currentState === 'void') {
      this.close.emit(true);
    }
  }

  closeForm(): void {
    this.currentState = 'void';
  }

  create(event): void {
    this.validateForm();
    if (!this.form.invalid) {
      if (this.isCreatingForm) {
        // Create new task
        let task = new Task();
        task.title = this.title.value;
        task.desc = this.desc.value;
        task.date = this.toDate(this.date.value);
        this.taskService.addTask(task)
          .subscribe(t => this.task = t);
      } else {
        // Update existing task
        this.task.title = this.title.value;
        this.task.desc = this.desc.value;
        this.task.date = this.toDate(this.date.value);
        this.taskService.updateTask(this.task)
          .subscribe(t => this.task = t);
      }
      this.closeForm();
    }
  }

  validateForm(): void {
    Object.keys(this.form.controls).forEach( field => {
      const control = this.form.get(field);
      control.markAsTouched({ onlySelf: true });
      control.markAsDirty({ onlySelf: true });
    });
  }

  fromDate(date: Date|string): NgbDateStruct {
    if (typeof date === 'string') {
      date = new Date(date);
    }
    return date ? {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    } : null;
  }

  toDate(date: NgbDateStruct): Date {
    return date ? new Date(date.year, date.month - 1, date.day) : null;
  }
}
