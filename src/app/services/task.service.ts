import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { plainToClass } from "class-transformer";

import { Task } from '../task/task';

const STORAGE_KEY = 'tasklist';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  tasks: Task[] = [];
  private behaviorSubjectTasks: BehaviorSubject<Task[]>;

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.tasks = this.storage.get(STORAGE_KEY);
    if (this.tasks === null) {
      this.storage.set(STORAGE_KEY, []);
      this.tasks = [];
    }
    this.tasks = this.tasks.map(v => {
      v.date = new Date(v.date);
      return plainToClass(Task, v);
    });
    this.behaviorSubjectTasks = new BehaviorSubject<Task[]>(this.tasks)
  }

  getTasks(): BehaviorSubject<Task[]> {
    return this.behaviorSubjectTasks;
  }

  setTasks(tasks: Task[]): BehaviorSubject<Task[]> {
    this.tasks = tasks;
    this.storage.set(STORAGE_KEY, tasks);
    return this.getTasks();
  }

  addTask(task: Task): Observable<Task> {
    this.tasks.push(task);
    this.behaviorSubjectTasks.next(this.tasks);
    this.storage.set(STORAGE_KEY, this.tasks);
    return of(task);
  }

  updateTask(task: Task): Observable<Task> {
    this.storage.set(STORAGE_KEY, this.tasks);
    return of(task);
  }

  closeTask(task: Task): Observable<Task> {
    task.done = true;
    this.storage.set(STORAGE_KEY, this.tasks);
    return of(task);
  }

  removeTask(task: Task): Observable<Task> {
    this.tasks = this.tasks.filter(t => t !== task);
    this.behaviorSubjectTasks.next(this.tasks);
    this.storage.set(STORAGE_KEY, this.tasks);
    return of(task);
  }
}



