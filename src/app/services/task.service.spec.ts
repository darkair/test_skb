import { TestBed } from '@angular/core/testing';
import { StorageServiceModule }     from 'angular-webstorage-service';
import { TaskService } from './task.service';

describe('TaskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StorageServiceModule,
      ],
    })
  });

  it('should be created', () => {
    const service: TaskService = TestBed.get(TaskService);
    expect(service).toBeTruthy();
  });
});
