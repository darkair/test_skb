import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DragulaModule }            from 'ng2-dragula';
import { StorageServiceModule }     from 'angular-webstorage-service';
import { TaskListComponent }        from './task-list.component';

describe('TaskListComponent', () => {
  let component: TaskListComponent;
  let fixture: ComponentFixture<TaskListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DragulaModule.forRoot(),
        StorageServiceModule
      ],
      declarations: [
        TaskListComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
