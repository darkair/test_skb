import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { Observable, of } from 'rxjs';

import { TaskService } from '../services/task.service';
import { Task } from '../task/task';

@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.less']
})
export class TaskListComponent implements OnInit {
  @Output() viewTask: EventEmitter<Task> = new EventEmitter();
  @Output() editTask: EventEmitter<Task> = new EventEmitter();

  tasks: Task[];
  tasksInProgress: Array<any>;
  tasksComplete: Array<any>;

  constructor(
    private taskService: TaskService,
    private dragulaService: DragulaService
  ) {}

  ngOnInit() {
    this.getTasks();

    this.dragulaService.dropModel("INPROGRESS")
      .subscribe(
        ({ sourceIndex, targetIndex }) => {
          const from = this.tasksInProgress[sourceIndex].idx;
          const to = this.tasksInProgress[targetIndex].idx;
          this.tasks.splice(to, 0, this.tasks.splice(from, 1)[0]);
          this.taskService.setTasks(this.tasks)
            .subscribe(tasks => {
              this.getTasks();
            });
        }
      );
    this.dragulaService.dropModel("COMPLETE")
      .subscribe(
        ({ sourceIndex, targetIndex }) => {
          const from = this.tasksComplete[sourceIndex].idx;
          const to = this.tasksComplete[targetIndex].idx;
          this.tasks.splice(to, 0, this.tasks.splice(from, 1)[0]);
          this.taskService.setTasks(this.tasks)
            .subscribe(tasks => {
              this.getTasks();
            });
        }
      );
  }

  getTasks(): void {
    this.taskService.getTasks()
      .subscribe(tasks => {
        this.tasks = tasks;

        let tmpTasks = tasks.map((t, idx) => {
          return {idx, task:t};
        });
        this.tasksInProgress = tmpTasks.filter(t => !t.task.done);
        this.tasksComplete = tmpTasks.filter(t => t.task.done);
      });
  }

  initTasks(tasks: Task[]): void {
  }

  daysToDeadline(task: Task): number {
    return task.date.getDate() - (new Date()).getDate();
  }

  view(idx): void {
    this.viewTask.emit(this.tasks[idx]);
  }

  edit(idx): void {
    this.editTask.emit(this.tasks[idx]);
  }

  close(idx): void {
    this.taskService.closeTask(this.tasks[idx])
      .subscribe(task => {
        this.getTasks();
      });
  }

  remove(idx): void {
    this.taskService.removeTask(this.tasks[idx])
      .subscribe(task => {
        this.getTasks();
      });
  }
}
